package com.cobradoc

import groovy.io.FileType

/**
 * Created by torr on 8/30/2018.
 */
class Main {
    private static def syllabaryMap = ["Ꭰ" : 0,
                                        "Ꭱ" : 0,
                                        "Ꭲ" : 0,
                                        "Ꭳ" : 0,
                                        "Ꭴ" : 0,
                                        "Ꭵ" : 0,
                                        "Ꭶ" : 0,
                                        "Ꭷ" : 0,
                                        "Ꭸ" : 0,
                                        "Ꭹ" : 0,
                                        "Ꭺ" : 0,
                                        "Ꭻ" : 0,
                                        "Ꭼ" : 0,
                                        "Ꭽ" : 0,
                                        "Ꭾ" : 0,
                                        "Ꭿ" : 0,
                                        "Ꮀ" : 0,
                                        "Ꮁ" : 0,
                                        "Ꮂ" : 0,
                                        "Ꮃ" : 0,
                                        "Ꮄ" : 0,
                                        "Ꮅ" : 0,
                                        "Ꮆ" : 0,
                                        "Ꮇ" : 0,
                                        "Ꮈ" : 0,
                                        "Ꮉ" : 0,
                                        "Ꮊ" : 0,
                                        "Ꮋ" : 0,
                                        "Ꮌ" : 0,
                                        "Ꮍ" : 0,
                                        "Ꮎ" : 0,
                                        "Ꮏ" : 0,
                                        "Ꮐ" : 0,
                                        "Ꮑ" : 0,
                                        "Ꮒ" : 0,
                                        "Ꮓ" : 0,
                                        "Ꮔ" : 0,
                                        "Ꮕ" : 0,
                                        "Ꮖ" : 0,
                                        "Ꮗ" : 0,
                                        "Ꮘ" : 0,
                                        "Ꮙ" : 0,
                                        "Ꮚ" : 0,
                                        "Ꮛ" : 0,
                                        "Ꮜ" : 0,
                                        "Ꮝ" : 0,
                                        "Ꮞ" : 0,
                                        "Ꮟ" : 0,
                                        "Ꮠ" : 0,
                                        "Ꮡ" : 0,
                                        "Ꮢ" : 0,
                                        "Ꮣ" : 0,
                                        "Ꮤ" : 0,
                                        "Ꮥ" : 0,
                                        "Ꮦ" : 0,
                                        "Ꮧ" : 0,
                                        "Ꮨ" : 0,
                                        "Ꮩ" : 0,
                                        "Ꮪ" : 0,
                                        "Ꮫ" : 0,
                                        "Ꮬ" : 0,
                                        "Ꮭ" : 0,
                                        "Ꮮ" : 0,
                                        "Ꮯ" : 0,
                                        "Ꮰ" : 0,
                                        "Ꮱ" : 0,
                                        "Ꮲ" : 0,
                                        "Ꮳ" : 0,
                                        "Ꮴ" : 0,
                                        "Ꮵ" : 0,
                                        "Ꮶ" : 0,
                                        "Ꮷ" : 0,
                                        "Ꮸ" : 0,
                                        "Ꮹ" : 0,
                                        "Ꮺ" : 0,
                                        "Ꮻ" : 0,
                                        "Ꮼ" : 0,
                                        "Ꮽ" : 0,
                                        "Ꮾ" : 0,
                                        "Ꮿ" : 0,
                                        "Ᏸ" : 0,
                                        "Ᏹ" : 0,
                                        "Ᏺ" : 0,
                                        "Ᏻ" : 0,
                                        "Ᏼ" : 0];

    public static void main(String[] args) {
        String arg1 = args[0];
        def f = new File(arg1)
        f.eachFile(FileType.FILES) {file ->
            def txt = file.getText("UTF-8")
            txt.each {
                if (syllabaryMap.containsKey(it)) {
                    int value = syllabaryMap.get(it);
                    syllabaryMap.put(it, (value + 1));
                }
            }
        }

        println syllabaryMap

        def report = new File(arg1+"\\report.txt");
        report.write("")
        def sb = new StringBuilder()
        sb << syllabaryMap
        report << sb
    }
}
